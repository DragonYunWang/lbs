import { natives } from './util/asyncNatives'

export const login = async function (options) {
  const ret = await natives.fetch(options)
  return ret
}